class pauta {
	constructor(argInstrumento = "Piano") {
		this.instrumento = argInstrumento;
		this.nome = argInstrumento;
		this.compassos = [];
		for (var i=0; i<numCompassos; i++) {
			var novoCompasso = new compasso();
			novoCompasso.compasso_definirNumeroCompasso(i+1);
			this.pauta_addCompasso(novoCompasso);
		}
		this.idPauta = "p"+idPautas.toString();
		idPautas++;
		document.getElementById("textoPautas").innerHTML+="<div id=\"texto"+this.idPauta+"\" class=\"pauta_nome\">"+this.pauta_getNome()+"</div>";
	}
	pauta_addCompasso(argCompasso) {
		this.compassos.push(argCompasso);
		//console.log(this.compassos.length);
		if (this.compassos.length>1) {
			this.compassos[this.compassos.length-1].compassoAnterior=this.compassos[this.compassos.length-2];
			this.compassos[this.compassos.length-2].compassoSeguinte=this.compassos[this.compassos.length-1];
		}
	}
	pauta_drawCompassos(argApagarId = false) {
		var stringCompassos="";
		for (var i=0; i<this.compassos.length; i++) {
			stringCompassos+=this.compassos[i].compasso_drawSelf();
		}
		for (var i=0; i<numCompassos; i++) {
			//document.getElementById("compassoGrade"+i).innerHTML+="<p>"+i+"</p>";
		}
		if (argApagarId) { document.getElementById(this.idPauta).innerHTML=stringCompassos; }
		return stringCompassos;
	}
	pauta_drawSelf() {
		var stringPauta=this.pauta_drawCompassos();
		compassosGrade[0].innerHTML+="<p>a</p>";
		return "<div id='"+this.pauta_id()+"' class='sistema'><div class='pauta'>"+this.pauta_drawNome()+stringPauta+"</div></div>";
	}
	pauta_drawInterno() {
		document.getElementById(this.pauta_id()).innerHTML="";
		var stringPauta="";
		stringPauta+="<div class='pauta'>"+this.pauta_drawNome()+this.pauta_drawCompassos()+"</div></div>";
		document.getElementById(this.pauta_id()).innerHTML=stringPauta;
	}
	pauta_drawNome() {
		return "<div class='pauta_nome'>"+this.pauta_getNome()+"</div>";
	}
	pauta_id() {
		return this.idPauta;
	}
	pauta_getNome() {
		return this.nome;
	}
	pauta_obterCompasso(argIdCompasso) {
		var compassoRetornado=null;
		for (var i = 0; i<this.compassos.length; i++) {
			if (this.compassos[i].compasso_id()==argIdCompasso) {
				compassoRetornado=this.compassos[i];
			}
		}
		return compassoRetornado;
	}
	pauta_computarNumeroCompassos() {
		for (var i=1; i<=this.compassos.length; i++) {
			this.compassos[i-1].compasso_definirNumeroCompasso(i);
			this.compassos[i-1].compasso_computarLargura();
		}
	}
}