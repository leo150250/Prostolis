//Interface do Usu�rio/////////////////////////////////////////////////////////////////////////////
function interface_iniciarInterface() {
	interface_iniciarBarraFerramentas();
	interface_iniciarMenusContexto();
	interface_iniciarGUIs();
	interface_iniciarPaletas();
	interface_alterarMenuContexto();
	funcao_menuNovaPartituraEmBranco();
	interface_selecionarEscritaManual('sm');
}
function interface_iniciarBarraFerramentas() {
	ferramenta="f_geral";
	barraFerramentas=document.getElementById('barraFerramentas');
	barraFerramentas.innerHTML="";
	var spanBarra="";
	spanBarra+="<a class='botao' title='Nova Partitura' onClick='funcao_menuNovaPartituraEmBranco()'>&#128196;</a>";
	spanBarra+="<a class='botao' title='Salvar' onClick=\"funcao_salvarMusicXML('Partitura.xml')\">&#128190;</a>";
	spanBarra+="<a class='botao' title='Carregar' onClick=''>&#128194;</a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' title='Adicionar pauta' onClick='funcao_menuAddPauta()'></a>";
	spanBarra+="<a class='botao' title='Adicionar compasso' onClick='funcao_menuAddCompasso()'></a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' id='f_geral' title='Sele��o geral' onClick=\"interface_selecionarFerramenta('f_geral')\" selecionado>&#8689;</a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='f_pauta' title='Editar pauta' onClick=\"interface_selecionarFerramenta('f_pauta')\">&#127932;</a>";
	spanBarra+="<a class='botao' id='f_compasso' title='Editar compasso' onClick=\"interface_selecionarFerramenta('f_compasso')\">&#9636;</a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='f_escManual' title='Escrita manual' onClick=\"interface_selecionarFerramenta('f_escManual')\">&#x270D;</a>";
	spanBarra+="<a class='botao' id='f_escDirecionada' title='Escrita direcionada' onClick=\"interface_selecionarFerramenta('f_escDirecionada')\">&#x2328;</a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' title='Ajuda' onClick=''>&#x2754;</a>";
	spanBarra+="<a class='botao' title='Sobre o Prostolis' onClick=\"interface_exibirModal('prostolis','Fechar')\"><img src='prostolis_bolo.png'></a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
}
function interface_iniciarMenusContexto() {
	menuContexto=null;
	menuContexto_geral=document.getElementById('menuDeContexto_geral');
	menuContexto_geral.innerHTML="";
	menuContexto_geral.innerHTML+="<a onmouseover=\"interface_showSubMenuContexto(true,this,'menuDeContexto_pauta')\">Pauta</a>";
	menuContexto_geral.innerHTML+="<a onmouseover=\"interface_showSubMenuContexto(true,this,'menuDeContexto_compasso')\">Compasso</a>";
	menuContexto_geral.innerHTML+="<a onmouseover=\"interface_hideSubMenuContexto()\">Outra op��o</a>";
	menuContexto_pauta=document.getElementById('menuDeContexto_pauta');
	menuContexto_pauta.innerHTML="";
	menuContexto_pauta.innerHTML+="<a>Mover acima</a>";
	menuContexto_pauta.innerHTML+="<a>Mover abaixo</a>";
	menuContexto_pauta.innerHTML+="<hr>";
	menuContexto_pauta.innerHTML+="<a onclick=\"funcao_alterarNomePauta()\">Alterar nome...</a>";
	menuContexto_pauta.innerHTML+="<a>Definir instrumento...</a>";
	menuContexto_pauta.innerHTML+="<a>Alterar clave inicial...</a>";
	menuContexto_compasso=document.getElementById('menuDeContexto_compasso');
	menuContexto_compasso.innerHTML="";
	menuContexto_compasso.innerHTML+="<a onclick=\"interface_exibirModal('claves')\">Alterar clave...</a>";
	menuContexto_compasso.innerHTML+="<a onclick=\"interface_exibirModal('formulaCompasso')\">Alterar f�rmula de compasso...</a>";
	menuContexto_compasso.innerHTML+="<a>Alterar tom...</a>";
	menuContexto_compasso.innerHTML+="<hr>";
	menuContexto_compasso.innerHTML+="<a>Definir repeti��o...</a>";
	menuContexto_compasso.innerHTML+="<hr>";
	menuContexto_compasso.innerHTML+="<a onClick=\"funcao_menuDelCompasso()\">Apagar compasso</a>";
}
function interface_iniciarPaletas() {
	paleta_escManual=document.getElementById('paleta_escManual');
	escritaManual="sb";
	paleta_escManual.innerHTML="";
	var spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_sf' title='Semifusa' onClick=\"interface_selecionarEscritaManual('sf')\" ><img class='nota_botao' src='partitura/partitura_figSf.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_f' title='Fusa' onClick=\"interface_selecionarEscritaManual('f')\" ><img class='nota_botao' src='partitura/partitura_figF.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sc' title='Semicolcheia' onClick=\"interface_selecionarEscritaManual('sc')\" ><img class='nota_botao' src='partitura/partitura_figSc.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_c' title='Colcheia' onClick=\"interface_selecionarEscritaManual('c')\" ><img class='nota_botao' src='partitura/partitura_figC.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sm' title='Sem�nima' onClick=\"interface_selecionarEscritaManual('sm')\" ><img class='nota_botao' src='partitura/partitura_figSm.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_m' title='M�nima' onClick=\"interface_selecionarEscritaManual('m')\" ><img class='nota_botao' src='partitura/partitura_figM.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sb' title='Semibreve' onClick=\"interface_selecionarEscritaManual('sb')\" ><img class='nota_botao' src='partitura/partitura_figSb.png'></a>";
	//spanBarra+="<a class='botao' id='paleta_escManual_b' title='Breve' onClick=\"interface_selecionarEscritaManual('b')\" ><img class='nota_botao' src='partitura/partitura_figB.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_borracha' title='Borracha' onClick=\"interface_selecionarEscritaManual('borracha')\" ></a>";
	spanBarra+="<br>";
	spanBarra+="<a class='botao' id='paleta_escManual_sfPausa' title='Semifusa - Pausa' onClick=\"interface_selecionarEscritaManual('sfPausa')\" ><img class='pausa_botao' src='partitura/partitura_figSfPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_fPausa' title='Fusa - Pausa' onClick=\"interface_selecionarEscritaManual('fPausa')\" ><img class='pausa_botao' src='partitura/partitura_figFPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_scPausa' title='Semicolcheia - Pausa' onClick=\"interface_selecionarEscritaManual('scPausa')\" ><img class='pausa_botao' src='partitura/partitura_figScPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_cPausa' title='Colcheia - Pausa' onClick=\"interface_selecionarEscritaManual('cPausa')\" ><img class='pausa_botao' src='partitura/partitura_figCPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_smPausa' title='Sem�nima - Pausa' onClick=\"interface_selecionarEscritaManual('smPausa')\" ><img class='pausa_botao' src='partitura/partitura_figSmPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_mPausa' title='M�nima - Pausa' onClick=\"interface_selecionarEscritaManual('mPausa')\" ><img class='pausa_botao' src='partitura/partitura_figMPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sbPausa' title='Semibreve - Pausa' onClick=\"interface_selecionarEscritaManual('sbPausa')\" ><img class='pausa_botao' src='partitura/partitura_figSbPausa.png'></a>";
	//spanBarra+="<a class='botao' id='paleta_escManual_bPausa' title='Breve - Pausa' onClick=\"interface_selecionarEscritaManual('bPausa')\" ><img class='pausa_botao' src='partitura/partitura_figBPausa.png'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
	escritaManual_acidente="";
	spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_dobradoBemol' title='Dobrado bemol'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_bemol' title='Bemol'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_bequadro' title='Bequadro'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sustenido' title='Sustenido'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dobradoSustenido' title='Dobrado sustenido'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
	escritaManual_aumento=0;
	spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_pontoAumento' title='Ponto de aumento'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_pontoAumento2' title='2 pontos de aumento'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_pontoAumento3' title='3 pontos de aumento'></a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='paleta_escManual_sincopa' title='Sincopar'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPPPP' title='Pianissississimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPPP' title='Pianississimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPP' title='Pianissimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinP' title='Piano'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinMP' title='Mezzo piano'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinMF' title='Mezzo forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinF' title='Forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFF' title='Fortissimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFFF' title='Fortississimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFFFF' title='Fortissississimo'></a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinSFZ' title='Sforzando'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFZ' title='Forzatto'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinSF' title='Subito forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFP' title='Forte piano'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPF' title='Piano forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinSP' title='Piano forte'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
}
function interface_iniciarGUIs() {
	GUIEscritaManual=document.getElementById('GUIEscritaManual');
	GUIEscritaManual.style.display="none";
	GUIJanelaModal=document.getElementById('GUIJanelaModal');
	GUIJanelaModal_janela=document.getElementById('GUIJanelaModal_janela');
	GUIJanelaModal.style.display="none";
}
function interface_selecionarFerramenta(argFerramenta) {
	document.getElementById('f_geral').removeAttribute("selecionado");
	document.getElementById('f_pauta').removeAttribute("selecionado");
	document.getElementById('f_compasso').removeAttribute("selecionado");
	document.getElementById('f_escManual').removeAttribute("selecionado");
	document.getElementById('f_escDirecionada').removeAttribute("selecionado");
	document.getElementById(argFerramenta).setAttribute("selecionado","");
	ferramenta=argFerramenta;
	interface_alterarMenuContexto();
}
function interface_selecionarEscritaManual(argEscritaManual) {
	document.getElementById('paleta_escManual_borracha').removeAttribute("selecionado");
	//document.getElementById('paleta_escManual_b').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sb').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_m').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sm').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_c').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sc').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_f').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sf').removeAttribute("selecionado");
	//document.getElementById('paleta_escManual_bPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sbPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_mPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_smPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_cPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_scPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_fPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sfPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_'+argEscritaManual).setAttribute("selecionado","");
	escritaManual=argEscritaManual;
	interface_definirEscritaManual();
}
function interface_alterarMenuContexto() {
	menuContexto_geral.style.display="none";
	menuContexto_pauta.style.display="none";
	menuContexto_compasso.style.display="none";
	GUIEscritaManual.style.display="none";
	paleta_escManual.style.display="none";
	switch (ferramenta) {
		case "f_geral": menuContexto=menuContexto_geral; break;
		case "f_pauta": menuContexto=menuContexto_pauta; break;
		case "f_compasso": menuContexto=menuContexto_compasso; break;
		case "f_escManual": if (compassoSelecionado !== null) { GUIEscritaManual.style.display = "block"; } paleta_escManual.style.display = "block"; break;
	}
}
function interface_showMenuContexto(argExibir = true,e) {
	if (argExibir) {
		origem = window.event || e;
		menuContexto.style.left=(origem.clientX+window.scrollX).toString()+"px";
		menuContexto.style.top=(origem.clientY+window.scrollY).toString()+"px";
		menuContexto.style.display="block";
	} else {
		menuContexto.style.display="none";
	}
}
function interface_showSubMenuContexto(argExibir = true,e,argMenu) {
	var subMenuContexto=document.getElementById(argMenu);
	if (argExibir) {
		interface_hideSubMenuContexto();
		origem = e;
		posicaoOrigem=origem.getBoundingClientRect();
		subMenuContexto.style.left=(posicaoOrigem.right+window.scrollX).toString()+"px";
		subMenuContexto.style.top=(posicaoOrigem.top+window.scrollY).toString()+"px";
		subMenuContexto.style.display="block";
	} else {
		subMenuContexto.style.display="none";
	}
}
function interface_hideSubMenuContexto() {
	interface_showSubMenuContexto(false,this,'menuDeContexto_pauta');
	interface_showSubMenuContexto(false,this,'menuDeContexto_compasso');
}
function interface_definirEscritaManual() {
	if (compassoSelecionado!==null) {
		var origem=document.getElementById(compassoSelecionado.compasso_id());
		var posicaoOrigem=origem.getBoundingClientRect();
		var areaNotas=compassoSelecionado.larguraInternoCompasso;
		GUIEscritaManual.style.left=(posicaoOrigem.left+areaNotas+window.scrollX).toString()+"px";
		GUIEscritaManual.style.top=(posicaoOrigem.top+window.scrollY).toString()+"px";
		//window.scrollTo(posicaoOrigem.left+window.scrollX,posicaoOrigem.top+window.scrollY)
		GUIEscritaManual.style.width=(posicaoOrigem.width-areaNotas).toString()+"px";
		for (var i=-11; i<=11; i++) {
			var GUIElemento=document.getElementById("GUIEscritaManual_"+i.toString());
			//var posicaoNotaGUI=compassoSelecionado.larguraInternoCompasso;
			//GUIElemento.innerHTML="<img src='partitura/partitura_fig"+escritaManual+".png' style='left: "+posicaoNotaGUI+"px'>";
			GUIElemento.innerHTML="<img src='partitura/partitura_fig"+escritaManual+".png' style='left: 0px'>";
		}
		if ((ferramenta == "f_escManual") && (escritaManual!=="borracha")) {
			GUIEscritaManual.style.display = "block";
		} else {
			GUIEscritaManual.style.display = "none";
		}
		//document.getElementById(compassoSelecionado.compasso_id()).style.width=(compassoSelecionado.larguraInternoCompasso).toString()+"px";
		funcao_computarLarguraCompassos(compassoSelecionado.numeroCompasso);
	}
}
function interface_clickCompasso(argIdCompasso) {
	if (compassoSelecionado!==null) {
		//document.getElementById(compassoSelecionado.compasso_id()).style.width=compassoSelecionado.larguraCompasso;
		compassoSelecionado.compasso_computarLargura();
	}
	pautaSelecionada=null;
	compassoSelecionado=null;
	compassoAnterior=null;
	idCompassoSelecionado="";
	for (var i=0; i<pautas.length; i++) {
		for (var j=0; j<pautas[i].compassos.length; j++) {
			document.getElementById(pautas[i].compassos[j].compasso_id()).removeAttribute("selecionado");
		}
	}
	for (var i=0; i<pautas.length; i++) {
		var checarCompasso = pautas[i].pauta_obterCompasso(argIdCompasso);
		var checarPauta = pautas[i];
		if (checarCompasso!==null) {
			compassoSelecionado=checarCompasso;
			pautaSelecionada=checarPauta;
			compassoAnterior=compassoSelecionado.compasso_getAnterior();
		}
	}
	if ((ferramenta=="f_geral") || (ferramenta=="f_compasso")) {
		idCompassoSelecionado=compassoSelecionado.compasso_id();
		document.getElementById(idCompassoSelecionado).setAttribute("selecionado","");
		interface_definirEscritaManual();
	} else if (ferramenta=="f_escManual") {
		idCompassoSelecionado=compassoSelecionado.compasso_id();
		document.getElementById(idCompassoSelecionado).setAttribute("selecionado","");
		interface_definirEscritaManual();
	} else if (ferramenta="f_pauta") {
		idCompassoSelecionado=pautaSelecionada.compassos[0].compasso_id();
		for (var i=0; i<pautaSelecionada.compassos.length; i++) {
			document.getElementById(pautaSelecionada.compassos[i].compasso_id()).setAttribute("selecionado","");
		}
	}
}
function interface_exibirModal(argJanela,argTextoCancelar = "Cancelar") {
	var botaoOk="";
	var conteudo="";
	var souSelecionado="";
	var textoCancelar=argTextoCancelar;
	switch (argJanela) {
		case "formulaCompasso": {
			GUIJanelaModal_janela.innerHTML="<span class='titulo'>Alterar f�rmula de compasso</span>";
			GUIJanelaModal_janela.innerHTML+="<p>N� de tempos no compasso: <input id='modal_temposCompasso' type='number' min='1' max='16' value='"+compassoSelecionado.temposCompasso+"'></p>";
			if (compassoSelecionado.temposSemibreve==1) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo="<a class='botao' name='modal_temposSemibreve' value='1' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">1</a>";
			if (compassoSelecionado.temposSemibreve==2) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='2' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">2</a>";
			if (compassoSelecionado.temposSemibreve==4) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='4' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">4</a>";
			if (compassoSelecionado.temposSemibreve==8) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='8' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">8</a>";
			if (compassoSelecionado.temposSemibreve==16) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='16' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">16</a>";
			GUIJanelaModal_janela.innerHTML+="<p>N� de tempos da semibreve: "+conteudo+"</p>";
			botaoOk="<a class='botao destaque' onClick=\"interface_aplicarModal_formulaCompasso()\">Aplicar f�rmula</a>";
		} break;
		case "claves": {
			GUIJanelaModal_janela.innerHTML="<span class='titulo'>Alterar clave</span>";
			GUIJanelaModal_janela.innerHTML+="<p>Selecione a clave a ser aplicada:</p>";
			if (compassoSelecionado.clave=="G") { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo="<a class='botao' name='modal_claves' value='G' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+"><img src='partitura/partitura_claveG.png'></a>";
			if (compassoSelecionado.clave=="F") { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_claves' value='F' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+"><img src='partitura/partitura_claveF.png'></a>";
			if (compassoSelecionado.clave=="C") { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_claves' value='C' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+"><img src='partitura/partitura_claveC.png'></a>";
			GUIJanelaModal_janela.innerHTML+="<p align='center'>"+conteudo+"</p>";
			botaoOk="<a class='botao destaque' onClick=\"interface_aplicarModal_claves()\">Aplicar clave</a>";
		} break;
		case "prostolis": {
			GUIJanelaModal_janela.innerHTML="<span class='titulo'>Sobre o Prostolis</span>";
			GUIJanelaModal_janela.innerHTML+="<p align='center'><img style='max-width: 100%' src='prostolis_logo.png'><br>Vers�o 0.1</p>";
			GUIJanelaModal_janela.innerHTML+="<p>Criado a partir de uma sadia brincadeira do MRC, o <b>Prostolis</b> agora � um programa baseado em WEB de edi��o de partituras, desenvolvido em 2018 por <a href='http://www.leandrogabriel.net/'>Leandro Gabriel</a>.</p>";
			GUIJanelaModal_janela.innerHTML+="<p><i>\"...Se algu�m a� tiver um manual desse programa... 'Prostoillan', 'Prost�le'... eu falo 'Prostuis', n�... Eerrm... Envia pra mim por favor, acho que o Denison deve ter, deve manjar desses programas a�...\"</i><br>~Emanuel Daniel In�cio (2014)</p>";
			prostolisAudio.currentTime=0;
			prostolisAudio.play();
		} break;
	}
	GUIJanelaModal_janela.innerHTML+="<p class='botoes_base'>"+botaoOk+"<a onClick=\"interface_fecharModal()\" class='botao'>"+textoCancelar+"</a></p>";
	GUIJanelaModal.style.display="block";
}
function interface_botaoRadio(argElemento) {
	var elementos=document.getElementsByName(argElemento.getAttribute("name"));
	for (var i=0; i<elementos.length; i++) {
		elementos[i].removeAttribute("selecionado");
	}
	argElemento.setAttribute("selecionado","");
}
function interface_aplicarModal_formulaCompasso() {
	var temposCompasso_alterar=parseInt(document.getElementById("modal_temposCompasso").value);
	var temposSemibreve_alterar=0;
	var elementos=document.getElementsByName("modal_temposSemibreve");
	for (var i=0; i<elementos.length; i++) {
		if (elementos[i].hasAttribute("selecionado")) {
			temposSemibreve_alterar=parseInt(elementos[i].getAttribute("value"));
		}
	}
	funcao_alterarFormulaCompasso(temposCompasso_alterar,temposSemibreve_alterar);
	interface_fecharModal();
}
function interface_aplicarModal_claves() {
	var clave_alterar="";
	var elementos=document.getElementsByName("modal_claves");
	for (var i=0; i<elementos.length; i++) {
		if (elementos[i].hasAttribute("selecionado")) {
			clave_alterar=elementos[i].getAttribute("value");
		}
	}
	funcao_alterarClave(clave_alterar);
	interface_fecharModal();
}
function interface_fecharModal() {
	prostolisAudio.pause();
	GUIJanelaModal.style.display="none";
}
//Comandos de Mouse e Teclado//////////////////////////////////////////////////////////////////////
if (document.addEventListener) {
	document.addEventListener('contextmenu', function(e) {
		origem = e.srcElement || e.target;
		if ((origem.classList.contains("compasso")) || (origem.parentElement.classList.contains("compasso"))) {
			e.preventDefault();
			origem.click();
			interface_showMenuContexto(true,e);
		}
	}, false);
	document.addEventListener('mouseup', function(e) {
		origem = e.srcElement || e.target;
		if (origem.parentElement!==null) {
			//Clique fora do menu de contexto faz fech�-lo
			if (origem.parentElement.id!=="menuDeContexto") { 
				interface_showMenuContexto(false,e);
				interface_hideSubMenuContexto();
			}
			//Clique em uma figura faz v�rias a��es de acordo com a ferramenta selecionada
			if ((origem.parentElement.classList.contains("compasso")) && (origem.classList.contains("figura"))) { 
				if ((ferramenta=='f_escManual') && (escritaManual=='borracha')) {
					alert("Apagar!");
				}
			}
		}
	}, false);
	document.addEventListener('keydown', function(e) {
		origem = e.keyCode || e.which || e.charCode;
	}, false);
	document.addEventListener('keyup', function(e) {
		origem = e.keyCode || e.which || e.charCode;
		if (e.altKey) {
			switch (String.fromCharCode(origem)) {
				case "1": interface_selecionarEscritaManual('sfPausa'); break;
				case "2": interface_selecionarEscritaManual('fPausa'); break;
				case "3": interface_selecionarEscritaManual('scPausa'); break;
				case "4": interface_selecionarEscritaManual('cPausa'); break;
				case "5": interface_selecionarEscritaManual('smPausa'); break;
				case "6": interface_selecionarEscritaManual('mPausa'); break;
				case "7": interface_selecionarEscritaManual('sbPausa'); break;
				case "8": interface_selecionarEscritaManual('bPausa'); break;
			}
		} else {
			switch (String.fromCharCode(origem)) {
				case "1": interface_selecionarEscritaManual('sf'); break;
				case "2": interface_selecionarEscritaManual('f'); break;
				case "3": interface_selecionarEscritaManual('sc'); break;
				case "4": interface_selecionarEscritaManual('c'); break;
				case "5": interface_selecionarEscritaManual('sm'); break;
				case "6": interface_selecionarEscritaManual('m'); break;
				case "7": interface_selecionarEscritaManual('sb'); break;
				case "8": interface_selecionarEscritaManual('b'); break;
			}
		}
	}, false);
}