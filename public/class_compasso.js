class compasso {
	constructor(argTemposCompasso = 4, argTemposSemibreve = 4, argClave = "G", argNumeroCompasso = 0) {
		this.temposCompasso = argTemposCompasso;
		this.temposSemibreve = argTemposSemibreve;
		this.maxEDUs = (1024*(4/this.temposSemibreve))*this.temposCompasso;
		this.EDUs = 0;
		this.clave = argClave;
		this.armadura = 0;
		this.notas = [];
		this.compassoAnterior = null;
		this.compassoSeguinte = null;
		//var notaTeste = new nota();
		//this.compasso_addNota(notaTeste);
		this.idCompasso = "c"+idCompassos.toString();
		idCompassos++;
		this.pausaCheia=true;
		this.numeroCompasso = argNumeroCompasso;
		this.larguraCompasso = 0;
		this.larguraInternoCompasso = 0;
	}
	compasso_addNota(argNota) {
		if (this.EDUs < this.maxEDUs) {
			this.notas.push(argNota);
			this.compasso_drawInterno();
			if (this.EDUs>=this.maxEDUs) {
				if (this.compassoSeguinte == null) {
					funcao_menuAddCompasso(1);
				}
				interface_clickCompasso(this.compassoSeguinte.compasso_id());
			}
		} else {
			interface_clickCompasso(this.compassoSeguinte.compasso_id());
		}
	}
	compasso_drawNotas(argApagarId = false) {
		var stringNotas="";
		this.EDUs = 0;
		if (this.notas.length==0) {
			this.pausaCheia=true;
		} else {
			this.pausaCheia=false;
			for (var i=0; i<this.notas.length; i++) {
				stringNotas+=this.notas[i].nota_drawSelf();
				this.EDUs+=this.notas[i].figura.edu;
				this.larguraInternoCompasso+=28;
			}
		}
		if (argApagarId) { document.getElementById(this.idCompasso).innerHTML=stringNotas; }
		return stringNotas;
	}
	compasso_drawSelf() {
		var stringCompasso="";
		stringCompasso+=this.compasso_drawInterno(false);
		var classePausa="";
		if (this.pausaCheia) { classePausa=" compasso_pausa"; }
		return "<div id='"+this.compasso_id()+"' title='"+this.compasso_id()+"' class='compasso"+classePausa+"' style='width: "+this.larguraCompasso.toString()+"px' onClick=interface_clickCompasso('"+this.compasso_id()+"')>"+stringCompasso+"</div>";
	}
	compasso_drawInterno(argLimpar = true) {
		var stringCompasso="";
		this.larguraInternoCompasso = 0;
		stringCompasso+=this.compasso_drawPropriedades();
		stringCompasso+=this.compasso_drawNotas();
		if (argLimpar) {
			document.getElementById(this.compasso_id()).innerHTML="";
			if (this.pausaCheia) {
				document.getElementById(this.compasso_id()).classList.add("compasso_pausa");
			} else {
				document.getElementById(this.compasso_id()).classList.remove("compasso_pausa");
			}
			document.getElementById(this.compasso_id()).innerHTML=stringCompasso;
			funcao_computarLarguraCompassos(this.numeroCompasso);
		} else {
			return stringCompasso;
		}
	}
	compasso_id() {
		return this.idCompasso;
	}
	compasso_getAnterior() {
		return this.compassoAnterior;
	}
	compasso_getSeguinte() {
		return this.compassoSeguinte;
	}
	compasso_getArmadura() {
		return this.armadura.toString();
	}
	compasso_getTemposCompasso() {
		return this.temposCompasso.toString();
	}
	compasso_getTemposSemibreve() {
		return this.temposSemibreve.toString();
	}
	compasso_getClave(argItem = "clave") {
		if (argItem=="clave") {
			return this.clave.toString();
		} else if (argItem=="linha") {
			if (this.clave=="G") {
				return "2";
			} else if (this.clave=="F") {
				return "4";
			} else if (this.clave=="C") {
				return "3";
			}
		}
	}
	compasso_drawPropriedades() {
		var stringPropriedades="";
		if (this.compassoAnterior==null) {
			stringPropriedades+=this.compasso_drawClave();
			stringPropriedades+=this.compasso_drawArmadura();
			stringPropriedades+=this.compasso_drawFormula();
		} else {
			if (this.compassoAnterior.clave!=this.clave) {
				stringPropriedades+=this.compasso_drawClave();
			}
			if (this.compassoAnterior.armadura!=this.armadura) {
				stringPropriedades+=this.compasso_drawArmadura();
			}
			if ((this.compassoAnterior.temposCompasso!=this.temposCompasso) || (this.compassoAnterior.temposSemibreve!=this.temposSemibreve)) {
				stringPropriedades+=this.compasso_drawFormula();
			}
		}
		return stringPropriedades;
	}
	compasso_drawClave() {
		this.larguraInternoCompasso+=29;
		return "<img src='partitura/partitura_clave"+this.clave+".png' class='clave'>";
	}
	compasso_drawArmadura() {
		var stringArmadura="";
		return stringArmadura;
	}
	compasso_drawFormula() {
		var stringFormula="";
		this.larguraInternoCompasso+=30;
		stringFormula+="<div class='formulaCompasso'>";
		stringFormula+="<img class='temposCompasso' src='partitura/partitura_formula"+this.temposCompasso.toString()+".png'>";
		stringFormula+="<img class='temposSemibreve' src='partitura/partitura_formula"+this.temposSemibreve.toString()+".png'>";
		stringFormula+="</div>";
		return stringFormula;
	}
	compasso_alterarFormula(argTemposCompasso = 4, argTemposSemibreve = 4) {
		this.temposCompasso = argTemposCompasso;
		this.temposSemibreve = argTemposSemibreve;
		this.maxEDUs = (1024*(4/this.temposSemibreve))*this.temposCompasso;
		this.compasso_drawInterno();
		if (this.compassoSeguinte!==null) {
			this.compassoSeguinte.compasso_alterarFormula(argTemposCompasso,argTemposSemibreve);
		}
	}
	compasso_alterarClave(argClave = "G") {
		this.clave=argClave;
		this.compasso_drawInterno();
		if (this.compassoSeguinte!==null) {
			this.compassoSeguinte.compasso_alterarClave(argClave);
		}
	}
	compasso_definirNumeroCompasso(argNumeroCompasso) {
		this.numeroCompasso = argNumeroCompasso;
		this.larguraCompasso = larguraCompassos[this.numeroCompasso-1];
	}
	compasso_computarLargura() {
		document.getElementById(this.compasso_id()).style.width="auto";
		this.larguraCompasso=document.getElementById(this.compasso_id()).getBoundingClientRect().width;
		//this.larguraCompasso=parseInt(document.getElementById(this.compasso_id()).style.width);
		if (larguraCompassos[this.numeroCompasso-1]<=this.larguraCompasso) {
			larguraCompassos[this.numeroCompasso-1]=this.larguraCompasso;
		}
		document.getElementById(this.compasso_id()).style.width=larguraCompassos[this.numeroCompasso-1].toString()+"px";
	}
}