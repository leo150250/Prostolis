class figura {
	constructor(argEdu = 256, argPausa = false) {
		this.edu = argEdu;
		this.pausa = argPausa;
		this.idFigura = "f"+idNotas.toString();
		this.nota = null;
		idNotas++;
	}
	figura_drawSelf() {
		var eduComputado=this.edu;
		var stringFigura="";
		if (eduComputado>=8192) {
			stringFigura="B";
		} else if (eduComputado>=4096) {
			stringFigura="Sb";
		} else if (eduComputado>=2048) {
			stringFigura="M";
		} else if (eduComputado>=1024) {
			stringFigura="Sm";
		} else if (eduComputado>=512) {
			stringFigura="C";
		} else if (eduComputado>=256) {
			stringFigura="Sc";
		} else if (eduComputado>=128) {
			stringFigura="F";
		} else if (eduComputado>=64) {
			stringFigura="Sf";
		}
		var altura=funcao_notaToStr(this.nota.nota_getAltura());
		var posicaoNota=0;
		if (this.nota.compasso.clave=="G") {
			switch (altura.charAt(0)) {
				case "B": posicaoNota=0; break;
				case "A": posicaoNota=5; break;
				case "G": posicaoNota=10; break;
				case "F": posicaoNota=15; break;
				case "E": posicaoNota=20; break;
				case "D": posicaoNota=25; break;
				case "C": posicaoNota=30; break;
			}
			switch (altura.charAt(1)) {
				case "#":
				case "b": break;
				default: posicaoNota+=(3-altura.charAt(1))*35;
			}	
		} else if (this.nota.compasso.clave=="F") {
			switch (altura.charAt(0)) {
				case "B": posicaoNota=-25; break;
				case "A": posicaoNota=-20; break;
				case "G": posicaoNota=-15; break;
				case "F": posicaoNota=-10; break;
				case "E": posicaoNota=-5; break;
				case "D": posicaoNota=0; break;
				case "C": posicaoNota=5; break;
			}
			switch (altura.charAt(1)) {
				case "#":
				case "b": break;
				default: posicaoNota+=(2-altura.charAt(1))*35;
			}	
		} else if (this.nota.compasso.clave=="C") {
			switch (altura.charAt(0)) {
				case "B": posicaoNota=-30; break;
				case "A": posicaoNota=-25; break;
				case "G": posicaoNota=-20; break;
				case "F": posicaoNota=-15; break;
				case "E": posicaoNota=-10; break;
				case "D": posicaoNota=-5; break;
				case "C": posicaoNota=0; break;
			}
			switch (altura.charAt(1)) {
				case "#":
				case "b": break;
				default: posicaoNota+=(3-altura.charAt(1))*35;
			}	
		}
		if (this.pausa) {
			stringFigura+="Pausa";
			posicaoNota=0;
		}
		var linhasSuplementares="";
		if (posicaoNota>=30) {
			linhasSuplementares=" ls inf";
			if (posicaoNota%10==5) {
				linhasSuplementares+="0";
			} else {
				linhasSuplementares+="1";
			}
		} else if (posicaoNota<=-30) {
			linhasSuplementares=" ls sup";
			if ((posicaoNota*-1)%10==5) {
				linhasSuplementares+="0";
			} else {
				linhasSuplementares+="1";
			}
		}
		return "<img id='"+this.figura_id()+"' class='figura"+linhasSuplementares+"' style='top: "+posicaoNota.toString()+"px' src='partitura/partitura_fig"+stringFigura+".png'>";
		//return "<img id='"+this.figura_id()+"' class='figura' style='top: 0px' src='partitura/partitura_fig"+stringFigura+".png'>";
	}
	figura_id() {
		return this.idFigura;
	}
	figura_definirNota(argNota) {
		this.nota=argNota;
	}
}