class nota {
	constructor(argFigura = new figura(), argAltura = "C3") {
		this.figura = argFigura;
		this.compasso = null;
		//console.log(typeof argAltura);
		if (typeof argAltura=="string") {
			this.altura = funcao_strToNota(argAltura);
		} else if (typeof argAltura=="number") {
			this.altura = argAltura;
		}
		this.notaAnterior = null;
		this.notaSeguinte = null;
	}
	nota_drawSelf() {
		return this.figura.figura_drawSelf();
	}
	nota_figuraId() {
		return this.figura.figura_id();
	}
	nota_getAltura() {
		return this.altura;
	}
	nota_definirCompasso(argCompasso) {
		this.compasso = argCompasso;
	}
}