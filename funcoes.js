//Funções//////////////////////////////////////////////////////////////////////////////////////////
function funcao_menuAddPauta() {
	console.log("funcao_menuAddPauta()");
	novaPauta=new pauta();
	pautas.push(novaPauta);
	funcao_drawCompassosGrade();
	//div_partitura.innerHTML+=novaPauta.pauta_drawSelf();
	/*for (var i=0; i<numCompassos; i++) {
		funcao_computarLarguraCompassos(i+1);
	}*/
}
function funcao_drawCompassosGrade(argNumCompasso=-1) {
	if (argNumCompasso==-1) {
		for (var i=0; i<compassosGrade.length; i++) {
			compassosGrade[i].innerHTML="";
			for (var j=0; j<pautas.length; j++) {
				compassosGrade[i].innerHTML+=pautas[j].compassos[i].compasso_drawSelf();
			}
		}
		funcao_detectaNovaLinha();
	} else {
		compassosGrade[argNumCompasso].innerHTML="";
		for (var j=0; j<pautas.length; j++) {
			compassosGrade[argNumCompasso].innerHTML+=pautas[j].compassos[argNumCompasso].compasso_drawSelf();
		}
	}
}
function funcao_detectaNovaLinha() {
	for (var i=0; i<compassosGrade.length; i++) {
		for (var j=0; j<pautas.length; j++) {
			pautas[j].compassos[i].compasso_computarLinha();
		}
	}
}
function funcao_menuAddCompasso(argNumCompassos = 0) {
	console.log("funcao_menuAddCompasso("+argNumCompassos+")");
	if (argNumCompassos==0) {
		numCompassosAdd=window.prompt("Informe o nº de compassos a serem adicionados ao final da partitura:","1");
	} else {
		numCompassosAdd=argNumCompassos;
	}
	if (numCompassosAdd=="" || parseInt(numCompassosAdd)=="NaN") {
		alert("Comando inválido. Insira um número!");
	} else if (numCompassosAdd==null) {
		//Nada. Apenas apertou em "Cancelar!"
	} else {
		for (var i=0; i<parseInt(numCompassosAdd); i++) {
			numCompassos++;
			larguraCompassos[numCompassos-1]=0;
			for (var j=0; j<pautas.length; j++) {
				var novoCompasso = new compasso();
				novoCompasso.compasso_definirNumeroCompasso(numCompassos);
				pautas[j].pauta_addCompasso(novoCompasso);
				pautas[j].pauta_drawInterno();
			}
		}
		for (var i=0; i<numCompassos; i++) {
			funcao_computarLarguraCompassos(i+1);
		}
	}
}
function funcao_menuDelCompasso() {
	console.log("funcao_menuDelCompasso()");
	var indiceCompasso=0;
	for (var i=0; i<pautaSelecionada.compassos.length; i++) {
		if (pautaSelecionada.compassos[i].compasso_id()==compassoSelecionado.compasso_id()) {
			indiceCompasso=i;
		}
	}
	for (var i=0; i<pautas.length; i++) {
		var arrumarCompassoAnterior=pautas[i].compassos[indiceCompasso].compassoAnterior;
		var arrumarCompassoSeguinte=pautas[i].compassos[indiceCompasso].compassoSeguinte;
		//console.log("ID apagar: "+compassoSelecionado.compasso_id());
		if (arrumarCompassoAnterior!==null) {
			arrumarCompassoAnterior.compassoSeguinte=arrumarCompassoSeguinte;
			//console.log("ID anterior: "+arrumarCompassoAnterior.compasso_id());
		}
		if (arrumarCompassoSeguinte!==null) {
			arrumarCompassoSeguinte.compassoAnterior=arrumarCompassoAnterior;
			//console.log("ID seguinte: "+arrumarCompassoSeguinte.compasso_id());
		}
		pautas[i].compassos.splice(indiceCompasso,1);
		pautas[i].pauta_drawInterno();
		pautas[i].pauta_computarNumeroCompassos();
	}
	//console.log("ID do compasso seguinte de "+arrumarCompassoAnterior.compasso_id()+": "+arrumarCompassoAnterior.compassoSeguinte.compasso_id());
	//console.log("ID do compasso anterior de "+arrumarCompassoSeguinte.compasso_id()+": "+arrumarCompassoSeguinte.compassoAnterior.compasso_id());
	compassoSelecionado=null;
	numCompassos--;
	for (var i=1; i<=numCompassos; i++) {
		funcao_computarLarguraCompassos(i);
	}
}
function funcao_menuNovaPartituraEmBranco() {
	console.log("funcao_menuNovaPartituraEmBranco()");
	zoomPagina=100;
	idCompassos=0;
	idNotas=0;
	idPautas=0;
	pautas = [];
	numCompassos=16;
	idCompassosGrade=0;
	compassosGrade = [];
	div_partitura.innerHTML="<div id=\"textoPautas\"></div>";
	larguraCompassos = [];
	for (var i=0; i<numCompassos; i++) {
		larguraCompassos[i]=0;
		novoCompassoGrade=document.createElement("div");
		novoCompassoGrade.id="compassoGrade"+idCompassosGrade;
		novoCompassoGrade.classList.add("compassoGrade");
		compassosGrade.push(novoCompassoGrade);
		div_partitura.appendChild(novoCompassoGrade);
		//div_partitura.innerHTML+="<div id=\"compassoGrade"+idCompassosGrade+"\" class=\"compassoGrade\"></div>";
		idCompassosGrade++;
	}
	idCompassoSelecionado="";
	pautaSelecionada=null;
	compassoSelecionado=null;
	compassoAnterior=null;
	interface_iniciarGUIs();
	funcao_menuAddPauta();
	interface_definirZoom("=");
}
function funcao_alterarClave(argClave = 'G', argCompasso = compassoSelecionado) {
	argCompasso.compasso_alterarClave(argClave);
}
function funcao_alterarFormulaCompasso(argTemposCompasso = 4, argTemposSemibreve = 4, argCompasso = compassoSelecionado) {
	argCompasso.compasso_alterarFormula(argTemposCompasso,argTemposSemibreve);
}
function funcao_alterarNomePauta(argPauta = pautaSelecionada) {
	var nomePauta=window.prompt("Informe o novo nome da pauta:",pautaSelecionada.pauta_getNome());
	if (nomePauta=="" || nomePauta==null) {
		//Nada. Apenas apertou em "Cancelar!"
	} else {
		pautaSelecionada.nome=nomePauta;
		pautaSelecionada.pauta_drawInterno();
	}
}
function funcao_computarLarguraCompassos(argNumeroCompasso) {
	console.log("funcao_computarLarguraCompassos("+argNumeroCompasso+")");
	//console.log(argNumeroCompasso.toString());
	larguraCompassos[argNumeroCompasso-1]=0;
	for (var i=0; i<pautas.length; i++) {
		var pauta=pautas[i];
		var compasso=pauta.compassos[argNumeroCompasso-1];
		compasso.compasso_computarLargura();
		//console.log(compasso.compasso_id());
	}
	for (var i=0; i<pautas.length; i++) {
		var pauta=pautas[i];
		var compasso=pauta.compassos[argNumeroCompasso-1];
		compasso.compasso_computarLargura();
	}
	interface_atualizarLayoutPagina();
}
function funcao_inserirNota(argPosicao) {
	var posicaoNota = funcao_strToNota("C0");
	if (compassoSelecionado.clave=="G") {
		switch (argPosicao) {
			case -11: posicaoNota = funcao_strToNota("F5"); break;
			case -10: posicaoNota = funcao_strToNota("E5"); break;
			case -9: posicaoNota = funcao_strToNota("D5"); break;
			case -8: posicaoNota = funcao_strToNota("C5"); break;
			case -7: posicaoNota = funcao_strToNota("B4"); break;
			case -6: posicaoNota = funcao_strToNota("A4"); break;
			case -5: posicaoNota = funcao_strToNota("G4"); break;
			case -4: posicaoNota = funcao_strToNota("F4"); break;
			case -3: posicaoNota = funcao_strToNota("E4"); break;
			case -2: posicaoNota = funcao_strToNota("D4"); break;
			case -1: posicaoNota = funcao_strToNota("C4"); break;
			case 0: posicaoNota = funcao_strToNota("B3"); break;
			case 1: posicaoNota = funcao_strToNota("A3"); break;
			case 2: posicaoNota = funcao_strToNota("G3"); break;
			case 3: posicaoNota = funcao_strToNota("F3"); break;
			case 4: posicaoNota = funcao_strToNota("E3"); break;
			case 5: posicaoNota = funcao_strToNota("D3"); break;
			case 6: posicaoNota = funcao_strToNota("C3"); break;
			case 7: posicaoNota = funcao_strToNota("B2"); break;
			case 8: posicaoNota = funcao_strToNota("A2"); break;
			case 9: posicaoNota = funcao_strToNota("G2"); break;
			case 10: posicaoNota = funcao_strToNota("F2"); break;
			case 11: posicaoNota = funcao_strToNota("E2"); break;
		}
	} else if (compassoSelecionado.clave=="F") {
		switch (argPosicao) {
			case -11: posicaoNota = funcao_strToNota("A3"); break;
			case -10: posicaoNota = funcao_strToNota("G3"); break;
			case -9: posicaoNota = funcao_strToNota("F3"); break;
			case -8: posicaoNota = funcao_strToNota("E3"); break;
			case -7: posicaoNota = funcao_strToNota("D3"); break;
			case -6: posicaoNota = funcao_strToNota("C3"); break;
			case -5: posicaoNota = funcao_strToNota("B2"); break;
			case -4: posicaoNota = funcao_strToNota("A2"); break;
			case -3: posicaoNota = funcao_strToNota("G2"); break;
			case -2: posicaoNota = funcao_strToNota("F2"); break;
			case -1: posicaoNota = funcao_strToNota("E2"); break;
			case 0: posicaoNota = funcao_strToNota("D2"); break;
			case 1: posicaoNota = funcao_strToNota("C2"); break;
			case 2: posicaoNota = funcao_strToNota("B1"); break;
			case 3: posicaoNota = funcao_strToNota("A1"); break;
			case 4: posicaoNota = funcao_strToNota("G1"); break;
			case 5: posicaoNota = funcao_strToNota("F1"); break;
			case 6: posicaoNota = funcao_strToNota("E1"); break;
			case 7: posicaoNota = funcao_strToNota("D1"); break;
			case 8: posicaoNota = funcao_strToNota("C1"); break;
			case 9: posicaoNota = funcao_strToNota("B0"); break;
			case 10: posicaoNota = funcao_strToNota("A0"); break;
			case 11: posicaoNota = funcao_strToNota("G0"); break;
		}
	} else if (compassoSelecionado.clave=="C") {
		switch (argPosicao) {
			case -11: posicaoNota = funcao_strToNota("G4"); break;
			case -10: posicaoNota = funcao_strToNota("F4"); break;
			case -9: posicaoNota = funcao_strToNota("E4"); break;
			case -8: posicaoNota = funcao_strToNota("D4"); break;
			case -7: posicaoNota = funcao_strToNota("C4"); break;
			case -6: posicaoNota = funcao_strToNota("B3"); break;
			case -5: posicaoNota = funcao_strToNota("A3"); break;
			case -4: posicaoNota = funcao_strToNota("G3"); break;
			case -3: posicaoNota = funcao_strToNota("F3"); break;
			case -2: posicaoNota = funcao_strToNota("E3"); break;
			case -1: posicaoNota = funcao_strToNota("D3"); break;
			case 0: posicaoNota = funcao_strToNota("C3"); break;
			case 1: posicaoNota = funcao_strToNota("B2"); break;
			case 2: posicaoNota = funcao_strToNota("A2"); break;
			case 3: posicaoNota = funcao_strToNota("G2"); break;
			case 4: posicaoNota = funcao_strToNota("F2"); break;
			case 5: posicaoNota = funcao_strToNota("E2"); break;
			case 6: posicaoNota = funcao_strToNota("D2"); break;
			case 7: posicaoNota = funcao_strToNota("C2"); break;
			case 8: posicaoNota = funcao_strToNota("B1"); break;
			case 9: posicaoNota = funcao_strToNota("A1"); break;
			case 10: posicaoNota = funcao_strToNota("G1"); break;
			case 11: posicaoNota = funcao_strToNota("F1"); break;
		}
	}
	switch (escritaManual_acidente) {
		case "": break;
		case "dobradoBemol": posicaoNota-=2; break;
		case "bemol": posicaoNota-=1; break;
		case "bequadro": {
			if (funcao_notaToStr(posicaoNota).indexOf("#")!==-1) {
				posicaoNota-=1;
			} else if (funcao_notaToStr(posicaoNota).indexOf("b")!==-1) {
				posicaoNota+=1;
			}
		} break;
		case "sustenido": posicaoNota+=1; break;
		case "dobradoSustenido": posicaoNota+=2; break;
	}
	var duracaoNota = 0;
	switch (escritaManual) {
		case "sf":
		case "sfPausa": {
			duracaoNota = 64;
		} break;
		case "f":
		case "fPausa": {
			duracaoNota = 128;
		} break;
		case "sc":
		case "scPausa": {
			duracaoNota = 256;
		} break;
		case "c":
		case "cPausa": {
			duracaoNota = 512;
		} break;
		case "sm":
		case "smPausa": {
			duracaoNota = 1024;
		} break;
		case "m":
		case "mPausa": {
			duracaoNota = 2048;
		} break;
		case "sb":
		case "sbPausa": {
			duracaoNota = 4096;
		} break;
		case "b":
		case "bPausa": {
			duracaoNota = 8192;
		} break;
	}
	var aumentoNota = duracaoNota/2;
	for (var i=0; i<escritaManual_aumento; i++) {
		duracaoNota+=aumentoNota;
		aumentoNota=aumentoNota/2;
	}
	var pausa = false;
	if (escritaManual.indexOf("Pausa")!==-1) {
		pausa = true;
	}
	var figuraInserir = new figura(duracaoNota,pausa);
	var notaInserir = new nota(figuraInserir,posicaoNota);
	figuraInserir.figura_definirNota(notaInserir);
	notaInserir.nota_definirCompasso(compassoSelecionado);
	compassoSelecionado.compasso_addNota(notaInserir);
	interface_definirEscritaManual();
}
function funcao_strToNota(argValor) {
	var varNota=0;
	var varOitava=0;
	switch (argValor.charAt(0)) {
		case "C": varNota=0; break;
		case "D": varNota=2; break;
		case "E": varNota=4; break;
		case "F": varNota=5; break;
		case "G": varNota=7; break;
		case "A": varNota=9; break;
		case "B": varNota=11; break;
	}
	switch (argValor.charAt(1)) {
		case "#": varNota++; break;
		case "b": varNota--; break;
		default: varOitava=parseInt(argValor.charAt(1));
	}
	if (argValor.length==3) {
		varOitava=parseInt(argValor.charAt(2));
	}
	return varNota+(varOitava*12);
}
function funcao_notaToStr(argValor) {
	var varNota="";
	var varOitava=0;
	switch (argValor%12) {
		case 0: varNota="C"; break;
		case 1: varNota="C#"; break;
		case 2: varNota="D"; break;
		case 3: varNota="D#"; break;
		case 4: varNota="E"; break;
		case 5: varNota="F"; break;
		case 6: varNota="F#"; break;
		case 7: varNota="G"; break;
		case 8: varNota="G#"; break;
		case 9: varNota="A"; break;
		case 10: varNota="A#"; break;
		case 11: varNota="B"; break;
	}
	varOitava=Math.floor(argValor/12);
	return varNota+varOitava.toString();
}
function funcao_salvarMusicXML(argNomeArquivo) {
	var prepararXML=funcao_prepararXML();
	var salvar = document.createElement('a');
	salvar.setAttribute('href', 'data:text/xml;charset=utf-8,' + encodeURIComponent(prepararXML));
	salvar.setAttribute('download', argNomeArquivo);
	salvar.style.display = 'none';
	document.body.appendChild(salvar);
	salvar.click();
	document.body.removeChild(salvar);
}
function funcao_prepararXML() {
	var XML="";
	//Header do XML:
	XML+="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>";
	XML+="<!DOCTYPE score-partwise PUBLIC";
	XML+=" \"-//Recordare//DTD MusicXML 2.0 Partwise//EN\"";
	XML+=" \"http://www.musicxml.org/dtds/partwise.dtd\">";
	//Início da partitura
	XML+="<score-partwise version=\"2.0\">";
	//Lista de partes
	XML+="<part-list>";
	for (var i=0; i<pautas.length; i++) {
		XML+="<score-part id=\""+pautas[i].pauta_id()+"\">";
		XML+="<part-name>"+pautas[i].pauta_getNome()+"</part-name>";
		XML+="</score-part>";
	}
	XML+="</part-list>";
	//Detalhe de cada parte
	for (var i=0; i<pautas.length; i++) {
		XML+="<part id=\""+pautas[i].pauta_id()+"\">";
		for (var j=0; j<numCompassos; j++) {
			XML+="<measure number=\""+(j+1).toString()+"\">";
			if (pautas[i].compassos[j].compassoAnterior==null) {
				XML+="<attributes>";
				XML+="<divisions>16</divisions>";
				XML+="<key>";
				XML+="<fifths>"+pautas[i].compassos[j].compasso_getArmadura()+"</fifths>";
				XML+="</key>";
				XML+="<time>";
				XML+="<beats>"+pautas[i].compassos[j].compasso_getTemposCompasso()+"</beats>";
				XML+="<beat-type>"+pautas[i].compassos[j].compasso_getTemposSemibreve()+"</beat-type>";
				XML+="</time>";
				XML+="<clef>";
				XML+="<sign>"+pautas[i].compassos[j].compasso_getClave()+"</sign>";
				XML+="<line>"+pautas[i].compassos[j].compasso_getClave("linha")+"</line>";
				XML+="</clef>";
				XML+="</attributes>";
			}
			for (var k=0; k<pautas[i].compassos[j].notas.length; k++) {
				XML+="<note>";
				XML+="<pitch>";
				XML+="<step>"+funcao_notaToStr(pautas[i].compassos[j].notas[k].nota_getAltura()).charAt(0)+"</step>";
				XML+="<octave>"+(parseInt(funcao_notaToStr(pautas[i].compassos[j].notas[k].nota_getAltura()).charAt(1))+1)+"</octave>";
				XML+="</pitch>";
				XML+="<duration>"+(pautas[i].compassos[j].notas[k].figura.edu/64).toString()+"</duration>";
				XML+="<type>"+funcao_eduToMusicXML(pautas[i].compassos[j].notas[k].figura.edu)+"</type>";
				XML+="</note>";
			}
			XML+="</measure>";
		}
		XML+="</part>";
	}
	XML+="</score-partwise>";
	return XML;
}
function funcao_eduToMusicXML(argValor) {
	var tipo="";
	switch (argValor) {
		case 4096: tipo="whole"; break;
		case 2048: tipo="half"; break;
		case 1024: tipo="quarter"; break;
		case 512: tipo="eighth"; break;
		case 256: tipo="16th"; break;
		case 128: tipo="32nd"; break;
		case 64: tipo="64th"; break;
	}
	return tipo;
}