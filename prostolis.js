//Criar constantes:
const div_partitura=document.getElementById("partitura");

//Carregar classes:
document.write("<script src='class_figura.js' type='text/javascript'></script>");
document.write("<script src='class_nota.js' type='text/javascript'></script>");
document.write("<script src='class_compasso.js' type='text/javascript'></script>");
document.write("<script src='class_pauta.js' type='text/javascript'></script>");
//Carregar interface:
document.write("<script src='interface.js' type='text/javascript'></script>");
//Carregar funções:
document.write("<script src='funcoes.js' type='text/javascript'></script>");
//Quando tudo estiver pronto, o prostolis.html executa o programa:
function programaOk() {
	interface_iniciarInterface();
	document.getElementById('loader').style.display="none";
	prostolisAudio=document.getElementById('prostolisAudio');
}
window.onerror = function (msg, url, lineNo, columnNo, error) {
    var string = msg.toLowerCase();
    var substring = "script error";
    if (string.indexOf(substring) > -1){
		programaErro(msg, url, lineNo, columnNo, error);
    } else {
        programaErro(msg, url, lineNo, columnNo, error);
    }
  return false;
};
function programaErro(argMsg, argUrl, argLineNo, argColumnNo, argError) {
	document.getElementById('loaderSpinner').style.display="none";
	document.getElementById('loader').innerHTML+="<div style='background-color: #F008; color: #FFF; display: inline-block; padding: 10px; border-radius: 5px'><p style='margin: 0px'><b>&#x274C; Houve um erro ao carregar a aplicação!</b></p><hr>\""+argMsg+"\" em "+argLineNo+","+argColumnNo+": "+argError+"</div>";
}