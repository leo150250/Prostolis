//Interface do Usuário/////////////////////////////////////////////////////////////////////////////
//Variáveis de interface
var zoomPagina=100;
var moverPagina=false;
var paginaOffsetX=0;
var paginaOffsetY=0;
var offsetX=0;
var offsetY=0;
const div_GUIBarraRolagemV=document.getElementById("GUIBarraRolagemV");
const div_GUIBarraRolagemH=document.getElementById("GUIBarraRolagemH");

//Funções//////////////////////////////////////////////////////////////////////////////////////////
function interface_iniciarInterface() {
	partituraVisualizacao='pagina';
	interface_iniciarBarraFerramentas();
	interface_iniciarMenusContexto();
	interface_iniciarGUIs();
	interface_iniciarPaletas();
	interface_alterarMenuContexto();
	funcao_menuNovaPartituraEmBranco();
	interface_selecionarEscritaManual('sm');
	interface_selecionarVisualizacao(partituraVisualizacao);
	interface_definirZoom("=");
}
function interface_iniciarBarraFerramentas() {
	ferramenta="f_geral";
	barraFerramentas=document.getElementById('barraFerramentas');
	barraFerramentas.innerHTML="";
	var spanBarra="";
	spanBarra+="<a class='botao' title='Nova Partitura' onClick='funcao_menuNovaPartituraEmBranco()'>&#128196;</a>";
	spanBarra+="<a class='botao' title='Salvar' onClick=\"funcao_salvarMusicXML('Partitura.xml')\">&#128190;</a>";
	spanBarra+="<a class='botao' title='Carregar' onClick=''>&#128194;</a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' id='visual_pagina' title='Visualização de página' onClick=\"interface_selecionarVisualizacao('pagina')\" selecionado>&#x1F4D1;</a>";
	spanBarra+="<a class='botao' id='visual_linear' title='Visualização linear' onClick=\"interface_selecionarVisualizacao('linear')\">&#x1F39E;</a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' title='Reduzir' onClick='interface_definirZoom(\"-\")'>&#x2796;</a>";
	spanBarra+="<p id='visual_zoom'>"+zoomPagina+"%</p>";
	spanBarra+="<a class='botao' title='Ampliar' onClick='interface_definirZoom(\"+\")'>&#x2795;</a>";
	spanBarra+="<a class='botao' title='Restaurar visualização' onClick='interface_definirZoom(\"=\")'>&#x1F50E;</a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' title='Adicionar pauta' onClick='funcao_menuAddPauta()'></a>";
	spanBarra+="<a class='botao' title='Adicionar compasso' onClick='funcao_menuAddCompasso()'></a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' id='f_mao' title='Mão' onClick=\"interface_selecionarFerramenta('f_mao')\">&#129306;</a>";
	spanBarra+="<a class='botao' id='f_geral' title='Seleção geral' onClick=\"interface_selecionarFerramenta('f_geral')\" selecionado>&#8689;</a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='f_pauta' title='Editar pauta' onClick=\"interface_selecionarFerramenta('f_pauta')\">&#127932;</a>";
	spanBarra+="<a class='botao' id='f_compasso' title='Editar compasso' onClick=\"interface_selecionarFerramenta('f_compasso')\">&#9636;</a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='f_escManual' title='Escrita manual' onClick=\"interface_selecionarFerramenta('f_escManual')\">&#x270D;</a>";
	spanBarra+="<a class='botao' id='f_escDirecionada' title='Escrita direcionada' onClick=\"interface_selecionarFerramenta('f_escDirecionada')\">&#x2328;</a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' title='Ajuda' onClick=''>&#x2754;</a>";
	spanBarra+="<a class='botao' title='Sobre o Prostolis' onClick=\"interface_exibirModal('prostolis','Fechar')\"><img src='prostolis_bolo.png'></a>";
	barraFerramentas.innerHTML+="<span>"+spanBarra+"</span>";
}
function interface_iniciarMenusContexto() {
	menuContexto=null;
	menuContexto_geral=document.getElementById('menuDeContexto_geral');
	menuContexto_geral.innerHTML="";
	menuContexto_geral.innerHTML+="<a onmouseover=\"interface_showSubMenuContexto(true,this,'menuDeContexto_pauta')\">Pauta</a>";
	menuContexto_geral.innerHTML+="<a onmouseover=\"interface_showSubMenuContexto(true,this,'menuDeContexto_compasso')\">Compasso</a>";
	menuContexto_geral.innerHTML+="<a onmouseover=\"interface_hideSubMenuContexto()\">Outra opção</a>";
	menuContexto_pauta=document.getElementById('menuDeContexto_pauta');
	menuContexto_pauta.innerHTML="";
	menuContexto_pauta.innerHTML+="<a>Mover acima</a>";
	menuContexto_pauta.innerHTML+="<a>Mover abaixo</a>";
	menuContexto_pauta.innerHTML+="<hr>";
	menuContexto_pauta.innerHTML+="<a onclick=\"funcao_alterarNomePauta()\">Alterar nome...</a>";
	menuContexto_pauta.innerHTML+="<a>Definir instrumento...</a>";
	menuContexto_pauta.innerHTML+="<a>Alterar clave inicial...</a>";
	menuContexto_compasso=document.getElementById('menuDeContexto_compasso');
	menuContexto_compasso.innerHTML="";
	menuContexto_compasso.innerHTML+="<a onclick=\"interface_exibirModal('claves')\">Alterar clave...</a>";
	menuContexto_compasso.innerHTML+="<a onclick=\"interface_exibirModal('formulaCompasso')\">Alterar fórmula de compasso...</a>";
	menuContexto_compasso.innerHTML+="<a>Alterar tom...</a>";
	menuContexto_compasso.innerHTML+="<hr>";
	menuContexto_compasso.innerHTML+="<a>Definir repetição...</a>";
	menuContexto_compasso.innerHTML+="<hr>";
	menuContexto_compasso.innerHTML+="<a onClick=\"funcao_menuDelCompasso()\">Apagar compasso</a>";
}
function interface_iniciarPaletas() {
	paleta_escManual=document.getElementById('paleta_escManual');
	escritaManual="sb";
	paleta_escManual.innerHTML="";
	var spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_sf' title='Semifusa' onClick=\"interface_selecionarEscritaManual('sf')\" ><img class='nota_botao' src='partitura/partitura_figSf.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_f' title='Fusa' onClick=\"interface_selecionarEscritaManual('f')\" ><img class='nota_botao' src='partitura/partitura_figF.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sc' title='Semicolcheia' onClick=\"interface_selecionarEscritaManual('sc')\" ><img class='nota_botao' src='partitura/partitura_figSc.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_c' title='Colcheia' onClick=\"interface_selecionarEscritaManual('c')\" ><img class='nota_botao' src='partitura/partitura_figC.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sm' title='Semínima' onClick=\"interface_selecionarEscritaManual('sm')\" ><img class='nota_botao' src='partitura/partitura_figSm.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_m' title='Mínima' onClick=\"interface_selecionarEscritaManual('m')\" ><img class='nota_botao' src='partitura/partitura_figM.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sb' title='Semibreve' onClick=\"interface_selecionarEscritaManual('sb')\" ><img class='nota_botao' src='partitura/partitura_figSb.png'></a>";
	//spanBarra+="<a class='botao' id='paleta_escManual_b' title='Breve' onClick=\"interface_selecionarEscritaManual('b')\" ><img class='nota_botao' src='partitura/partitura_figB.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_borracha' title='Borracha' onClick=\"interface_selecionarEscritaManual('borracha')\" ></a>";
	spanBarra+="<br>";
	spanBarra+="<a class='botao' id='paleta_escManual_sfPausa' title='Semifusa - Pausa' onClick=\"interface_selecionarEscritaManual('sfPausa')\" ><img class='pausa_botao' src='partitura/partitura_figSfPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_fPausa' title='Fusa - Pausa' onClick=\"interface_selecionarEscritaManual('fPausa')\" ><img class='pausa_botao' src='partitura/partitura_figFPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_scPausa' title='Semicolcheia - Pausa' onClick=\"interface_selecionarEscritaManual('scPausa')\" ><img class='pausa_botao' src='partitura/partitura_figScPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_cPausa' title='Colcheia - Pausa' onClick=\"interface_selecionarEscritaManual('cPausa')\" ><img class='pausa_botao' src='partitura/partitura_figCPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_smPausa' title='Semínima - Pausa' onClick=\"interface_selecionarEscritaManual('smPausa')\" ><img class='pausa_botao' src='partitura/partitura_figSmPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_mPausa' title='Mínima - Pausa' onClick=\"interface_selecionarEscritaManual('mPausa')\" ><img class='pausa_botao' src='partitura/partitura_figMPausa.png'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sbPausa' title='Semibreve - Pausa' onClick=\"interface_selecionarEscritaManual('sbPausa')\" ><img class='pausa_botao' src='partitura/partitura_figSbPausa.png'></a>";
	//spanBarra+="<a class='botao' id='paleta_escManual_bPausa' title='Breve - Pausa' onClick=\"interface_selecionarEscritaManual('bPausa')\" ><img class='pausa_botao' src='partitura/partitura_figBPausa.png'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
	escritaManual_acidente="";
	spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_dobradoBemol' title='Dobrado bemol'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_bemol' title='Bemol'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_bequadro' title='Bequadro'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_sustenido' title='Sustenido'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dobradoSustenido' title='Dobrado sustenido'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
	escritaManual_aumento=0;
	spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_pontoAumento' title='Ponto de aumento'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_pontoAumento2' title='2 pontos de aumento'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_pontoAumento3' title='3 pontos de aumento'></a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='paleta_escManual_sincopa' title='Sincopar'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
	spanBarra="";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPPPP' title='Pianissississimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPPP' title='Pianississimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPP' title='Pianissimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinP' title='Piano'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinMP' title='Mezzo piano'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinMF' title='Mezzo forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinF' title='Forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFF' title='Fortissimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFFF' title='Fortississimo'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFFFF' title='Fortissississimo'></a>";
	spanBarra+="<hr>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinSFZ' title='Sforzando'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFZ' title='Forzatto'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinSF' title='Subito forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinFP' title='Forte piano'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinPF' title='Piano forte'></a>";
	spanBarra+="<a class='botao' id='paleta_escManual_dinSP' title='Piano forte'></a>";
	paleta_escManual.innerHTML+="<span>"+spanBarra+"</span>";
}
function interface_iniciarGUIs() {
	GUIEscritaManual=document.getElementById('GUIEscritaManual');
	GUIEscritaManual.style.display="none";
	GUIJanelaModal=document.getElementById('GUIJanelaModal');
	GUIJanelaModal_janela=document.getElementById('GUIJanelaModal_janela');
	GUIJanelaModal.style.display="none";
}
function interface_selecionarVisualizacao(argVisualizacao) {
	document.getElementById('visual_pagina').removeAttribute("selecionado");
	document.getElementById('visual_linear').removeAttribute("selecionado");
	partituraVisualizacao=argVisualizacao;
	if (argVisualizacao=='pagina') {
		for (var i=0; i<pautas.length; i++) {
			//document.getElementById(pautas[i].pauta_id()).style.flexWrap="wrap";
			//document.getElementById(pautas[i].pauta_id()).getElementsByClassName("pauta")[0].style.flexWrap="wrap";
		}
		div_partitura.style.flexWrap="wrap";
		document.body.style.background="rgb(230, 228, 240)";
		div_partitura.style.width="210mm";
		div_partitura.style.height="297mm";
		div_partitura.style.backgroundImage="url('fundo.jpg')";
		div_partitura.style.boxShadow="5px 5px 10px 2px rgba(0,0,0,0.2)";
	}
	if (argVisualizacao=='linear') {
		for (var i=0; i<pautas.length; i++) {
			//document.getElementById(pautas[i].pauta_id()).style.flexWrap="nowrap";
			//document.getElementById(pautas[i].pauta_id()).getElementsByClassName("pauta")[0].style.flexWrap="nowrap";
		}
		div_partitura.style.flexWrap="nowrap";
		document.body.style.background="rgb(255, 255, 255)";
		div_partitura.style.width="auto";
		div_partitura.style.height="auto";
		div_partitura.style.backgroundImage="none";
		div_partitura.style.boxShadow="none";
	}
	for (var i=0; i<numCompassos; i++) {
		//funcao_computarLarguraCompassos(i+1);
	}
	document.getElementById('visual_'+argVisualizacao).setAttribute("selecionado","");
	funcao_detectaNovaLinha();
	interface_ajustaPosicaoPagina();
}
function interface_selecionarFerramenta(argFerramenta) {
	document.getElementById('f_mao').removeAttribute("selecionado");
	document.getElementById('f_geral').removeAttribute("selecionado");
	document.getElementById('f_pauta').removeAttribute("selecionado");
	document.getElementById('f_compasso').removeAttribute("selecionado");
	document.getElementById('f_escManual').removeAttribute("selecionado");
	document.getElementById('f_escDirecionada').removeAttribute("selecionado");
	document.getElementById(argFerramenta).setAttribute("selecionado","");
	ferramenta=argFerramenta;
	if (ferramenta=="f_mao") {
		div_partitura.style.cursor="grab";
	} else {
		div_partitura.style.cursor="unset";
	}
	interface_alterarMenuContexto();
}
function interface_selecionarEscritaManual(argEscritaManual) {
	document.getElementById('paleta_escManual_borracha').removeAttribute("selecionado");
	//document.getElementById('paleta_escManual_b').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sb').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_m').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sm').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_c').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sc').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_f').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sf').removeAttribute("selecionado");
	//document.getElementById('paleta_escManual_bPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sbPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_mPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_smPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_cPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_scPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_fPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_sfPausa').removeAttribute("selecionado");
	document.getElementById('paleta_escManual_'+argEscritaManual).setAttribute("selecionado","");
	escritaManual=argEscritaManual;
	interface_definirEscritaManual();
}
function interface_alterarMenuContexto() {
	menuContexto_geral.style.display="none";
	menuContexto_pauta.style.display="none";
	menuContexto_compasso.style.display="none";
	GUIEscritaManual.style.display="none";
	paleta_escManual.style.display="none";
	switch (ferramenta) {
		case "f_mao": menuContexto=null; break;
		case "f_geral": menuContexto=menuContexto_geral; break;
		case "f_pauta": menuContexto=menuContexto_pauta; break;
		case "f_compasso": menuContexto=menuContexto_compasso; break;
		case "f_escManual": if (compassoSelecionado !== null) { GUIEscritaManual.style.display = "block"; } paleta_escManual.style.display = "block"; break;
	}
	interface_ajustaBarrasGUI();
}
function interface_showMenuContexto(argExibir = true,e) {
	if (menuContexto!==null) {
		if (argExibir) {
			origem = window.event || e;
			menuContexto.style.left=(origem.clientX+window.scrollX).toString()+"px";
			menuContexto.style.top=(origem.clientY+window.scrollY).toString()+"px";
			menuContexto.style.display="block";
		} else {
			menuContexto.style.display="none";
		}
	}
}
function interface_showSubMenuContexto(argExibir = true,e,argMenu) {
	var subMenuContexto=document.getElementById(argMenu);
	if (argExibir) {
		interface_hideSubMenuContexto();
		origem = e;
		posicaoOrigem=origem.getBoundingClientRect();
		subMenuContexto.style.left=(posicaoOrigem.right+window.scrollX).toString()+"px";
		subMenuContexto.style.top=(posicaoOrigem.top+window.scrollY).toString()+"px";
		subMenuContexto.style.display="block";
	} else {
		subMenuContexto.style.display="none";
	}
}
function interface_hideSubMenuContexto() {
	interface_showSubMenuContexto(false,this,'menuDeContexto_pauta');
	interface_showSubMenuContexto(false,this,'menuDeContexto_compasso');
}
function interface_definirEscritaManual() {
	if (compassoSelecionado!==null) {
		var origem=document.getElementById(compassoSelecionado.compasso_id());
		var posicaoOrigem=origem.getBoundingClientRect();
		var areaNotas=compassoSelecionado.larguraInternoCompasso;
		GUIEscritaManual.style.left=(posicaoOrigem.left+areaNotas+window.scrollX).toString()+"px";
		GUIEscritaManual.style.top=(posicaoOrigem.top+window.scrollY).toString()+"px";
		//window.scrollTo(posicaoOrigem.left+window.scrollX,posicaoOrigem.top+window.scrollY)
		GUIEscritaManual.style.width=(posicaoOrigem.width-areaNotas).toString()+"px";
		for (var i=-11; i<=11; i++) {
			var GUIElemento=document.getElementById("GUIEscritaManual_"+i.toString());
			//var posicaoNotaGUI=compassoSelecionado.larguraInternoCompasso;
			//GUIElemento.innerHTML="<img src='partitura/partitura_fig"+escritaManual+".png' style='left: "+posicaoNotaGUI+"px'>";
			GUIElemento.innerHTML="<img src='partitura/partitura_fig"+escritaManual+".png' style='left: 0px'>";
		}
		if ((ferramenta == "f_escManual") && (escritaManual!=="borracha")) {
			GUIEscritaManual.style.display = "block";
		} else {
			GUIEscritaManual.style.display = "none";
		}
		//document.getElementById(compassoSelecionado.compasso_id()).style.width=(compassoSelecionado.larguraInternoCompasso).toString()+"px";
		//funcao_computarLarguraCompassos(compassoSelecionado.numeroCompasso);
		funcao_detectaNovaLinha();
	}
}
function interface_clickCompasso(argIdCompasso) {
	if (compassoSelecionado!==null) {
		//document.getElementById(compassoSelecionado.compasso_id()).style.width=compassoSelecionado.larguraCompasso;
		//compassoSelecionado.compasso_computarLargura();
	}
	pautaSelecionada=null;
	compassoSelecionado=null;
	compassoAnterior=null;
	idCompassoSelecionado="";
	for (var i=0; i<pautas.length; i++) {
		for (var j=0; j<pautas[i].compassos.length; j++) {
			document.getElementById(pautas[i].compassos[j].compasso_id()).removeAttribute("selecionado");
		}
	}
	for (var i=0; i<pautas.length; i++) {
		var checarCompasso = pautas[i].pauta_obterCompasso(argIdCompasso);
		var checarPauta = pautas[i];
		if (checarCompasso!==null) {
			compassoSelecionado=checarCompasso;
			pautaSelecionada=checarPauta;
			compassoAnterior=compassoSelecionado.compasso_getAnterior();
		}
	}
	if ((ferramenta=="f_geral") || (ferramenta=="f_compasso")) {
		idCompassoSelecionado=compassoSelecionado.compasso_id();
		document.getElementById(idCompassoSelecionado).setAttribute("selecionado","");
		interface_definirEscritaManual();
	} else if (ferramenta=="f_escManual") {
		idCompassoSelecionado=compassoSelecionado.compasso_id();
		document.getElementById(idCompassoSelecionado).setAttribute("selecionado","");
		interface_definirEscritaManual();
	} else if (ferramenta=="f_pauta") {
		idCompassoSelecionado=pautaSelecionada.compassos[0].compasso_id();
		for (var i=0; i<pautaSelecionada.compassos.length; i++) {
			document.getElementById(pautaSelecionada.compassos[i].compasso_id()).setAttribute("selecionado","");
		}
	} else if (ferramenta=="f_mao") {
		//Faz nada. Selecione ninguém!
	}
}
function interface_exibirModal(argJanela,argTextoCancelar = "Cancelar") {
	var botaoOk="";
	var conteudo="";
	var souSelecionado="";
	var textoCancelar=argTextoCancelar;
	switch (argJanela) {
		case "formulaCompasso": {
			GUIJanelaModal_janela.innerHTML="<span class='titulo'>Alterar fórmula de compasso</span>";
			GUIJanelaModal_janela.innerHTML+="<p>Nº de tempos no compasso: <input id='modal_temposCompasso' type='number' min='1' max='16' value='"+compassoSelecionado.temposCompasso+"'></p>";
			if (compassoSelecionado.temposSemibreve==1) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo="<a class='botao' name='modal_temposSemibreve' value='1' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">1</a>";
			if (compassoSelecionado.temposSemibreve==2) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='2' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">2</a>";
			if (compassoSelecionado.temposSemibreve==4) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='4' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">4</a>";
			if (compassoSelecionado.temposSemibreve==8) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='8' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">8</a>";
			if (compassoSelecionado.temposSemibreve==16) { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_temposSemibreve' value='16' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+">16</a>";
			GUIJanelaModal_janela.innerHTML+="<p>Nº de tempos da semibreve: "+conteudo+"</p>";
			botaoOk="<a class='botao destaque' onClick=\"interface_aplicarModal_formulaCompasso()\">Aplicar fórmula</a>";
		} break;
		case "claves": {
			GUIJanelaModal_janela.innerHTML="<span class='titulo'>Alterar clave</span>";
			GUIJanelaModal_janela.innerHTML+="<p>Selecione a clave a ser aplicada:</p>";
			if (compassoSelecionado.clave=="G") { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo="<a class='botao' name='modal_claves' value='G' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+"><img src='partitura/partitura_claveG.png'></a>";
			if (compassoSelecionado.clave=="F") { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_claves' value='F' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+"><img src='partitura/partitura_claveF.png'></a>";
			if (compassoSelecionado.clave=="C") { souSelecionado="selecionado"; } else { souSelecionado=""; }
			conteudo+="<a class='botao' name='modal_claves' value='C' onClick=\"interface_botaoRadio(this)\" "+souSelecionado+"><img src='partitura/partitura_claveC.png'></a>";
			GUIJanelaModal_janela.innerHTML+="<p align='center'>"+conteudo+"</p>";
			botaoOk="<a class='botao destaque' onClick=\"interface_aplicarModal_claves()\">Aplicar clave</a>";
		} break;
		case "prostolis": {
			GUIJanelaModal_janela.innerHTML="<span class='titulo'>Sobre o Prostolis</span>";
			GUIJanelaModal_janela.innerHTML+="<p align='center'><img style='max-width: 100%' src='prostolis_logo.png'><br>Versão 0.1</p>";
			GUIJanelaModal_janela.innerHTML+="<p>Criado a partir de uma sadia brincadeira do MRC, o <b>Prostolis</b> agora é um programa baseado em WEB de edição de partituras, desenvolvido em 2018 por <a href='http://www.leandrogabriel.net/'>Leandro Gabriel</a>.</p>";
			GUIJanelaModal_janela.innerHTML+="<p><i>\"...Se alguém aí tiver um manual desse programa... 'Prostoillan', 'Prostóle'... eu falo 'Prostuis', né... Eerrm... Envia pra mim por favor, acho que o Denison deve ter, deve manjar desses programas aí...\"</i><br>~Emanuel Daniel Inácio (2014)</p>";
			prostolisAudio.currentTime=0;
			prostolisAudio.play();
		} break;
	}
	GUIJanelaModal_janela.innerHTML+="<p class='botoes_base'>"+botaoOk+"<a onClick=\"interface_fecharModal()\" class='botao'>"+textoCancelar+"</a></p>";
	GUIJanelaModal.style.display="block";
}
function interface_botaoRadio(argElemento) {
	var elementos=document.getElementsByName(argElemento.getAttribute("name"));
	for (var i=0; i<elementos.length; i++) {
		elementos[i].removeAttribute("selecionado");
	}
	argElemento.setAttribute("selecionado","");
}
function interface_aplicarModal_formulaCompasso() {
	var temposCompasso_alterar=parseInt(document.getElementById("modal_temposCompasso").value);
	var temposSemibreve_alterar=0;
	var elementos=document.getElementsByName("modal_temposSemibreve");
	for (var i=0; i<elementos.length; i++) {
		if (elementos[i].hasAttribute("selecionado")) {
			temposSemibreve_alterar=parseInt(elementos[i].getAttribute("value"));
		}
	}
	funcao_alterarFormulaCompasso(temposCompasso_alterar,temposSemibreve_alterar);
	interface_fecharModal();
}
function interface_aplicarModal_claves() {
	var clave_alterar="";
	var elementos=document.getElementsByName("modal_claves");
	for (var i=0; i<elementos.length; i++) {
		if (elementos[i].hasAttribute("selecionado")) {
			clave_alterar=elementos[i].getAttribute("value");
		}
	}
	funcao_alterarClave(clave_alterar);
	interface_fecharModal();
}
function interface_fecharModal() {
	prostolisAudio.pause();
	GUIJanelaModal.style.display="none";
}
function interface_atualizarLayoutPagina() {
	for (var i=0; i<pautas.length; i++) {
		var posicaoYSistema=null;
		for (var j=0; j<pautas[i].compassos.length; j++) {
			var compassoYSistema=document.getElementById(pautas[i].compassos[j].compasso_id());
			if (posicaoYSistema==null) {
				posicaoYSistema=compassoYSistema.getBoundingClientRect().y;
			} else {
				if (posicaoYSistema!==compassoYSistema.getBoundingClientRect().y) {
					posicaoYSistema=compassoYSistema.getBoundingClientRect().y;
					pautas[i].compassos[j].novaLinha=true;
					pautas[i].compassos[j].compasso_drawInterno(true,false);
				} else {
					pautas[i].compassos[j].novaLinha=false;
					pautas[i].compassos[j].compasso_drawInterno(true,false);
				}
			}
		}
	}
	if (partituraVisualizacao=='pagina') {
		for (var i=0; i<pautas.length; i++) {
			//document.getElementById(pautas[i].pauta_id()).style.flexWrap="wrap";
			//document.getElementById(pautas[i].pauta_id()).getElementsByClassName("pauta")[0].style.flexWrap="wrap";
		}
	}
	if (partituraVisualizacao=='linear') {
		for (var i=0; i<pautas.length; i++) {
			//document.getElementById(pautas[i].pauta_id()).style.flexWrap="wrap";
			//document.getElementById(pautas[i].pauta_id()).getElementsByClassName("pauta")[0].style.flexWrap="nowrap";
		}
	}
}
function interface_definirZoom(argDirecao) {
	if (argDirecao=="-") {
		zoomPagina=zoomPagina-10;
	} else if (argDirecao=="+") {
		zoomPagina=zoomPagina+10;
	} else if (argDirecao=="=") {
		zoomPagina=100;
	}
	document.getElementById("visual_zoom").innerHTML=zoomPagina+"%";
	interface_ajustaPosicaoPagina();
}
//Define a função dos botões das barras de rolagem.
//argRolagem: string, recebe uma string de dois caracteres, contendo "v"/"h" (vertical, horizontal) e "-"/"+" (negativo, positivo).
function interface_rolarPagina(argRolagem) {
	var quantia=0;
	if (argRolagem.charAt(1)=="-") {
		quantia=50;
	} else if (argRolagem.charAt(1)=="+") {
		quantia=-50;
	}
	if (argRolagem.charAt(0)=="v") {
		paginaOffsetY+=quantia;
	} else if (argRolagem.charAt(0)=="h") {
		paginaOffsetX+=quantia;
	}
	interface_ajustaPosicaoPagina();
}

//Ajusta a posicação da página de acordo com as informações de offset e zoom. Recomenda-se chamar essa função sempre que há uma mudança na exibição da interface.
function interface_ajustaPosicaoPagina() {
	var stringTransform="";
	stringTransform+=" translate("+paginaOffsetX+"px,"+paginaOffsetY+"px)";
	stringTransform+=" scale("+(zoomPagina/100)+")";
	div_partitura.style.transform=stringTransform;
	div_partitura.style.transformOrigin=((window.innerWidth/2)-div_partitura.getBoundingClientRect().x)/(zoomPagina/100)+"px "+((window.innerHeight/2)-div_partitura.getBoundingClientRect().y)/(zoomPagina/100)+"px";
	if (div_partitura.getBoundingClientRect().x>window.innerWidth/2) {
		paginaOffsetX-=div_partitura.getBoundingClientRect().x-window.innerWidth/2;
	}
	if (div_partitura.getBoundingClientRect().y>window.innerHeight/2) {
		paginaOffsetY-=div_partitura.getBoundingClientRect().y-window.innerHeight/2;
	}
	if (div_partitura.getBoundingClientRect().x+div_partitura.getBoundingClientRect().width<window.innerWidth/2) {
		paginaOffsetX-=(div_partitura.getBoundingClientRect().x+div_partitura.getBoundingClientRect().width)-window.innerWidth/2;
	}
	if (div_partitura.getBoundingClientRect().y+div_partitura.getBoundingClientRect().height<window.innerHeight/2) {
		paginaOffsetY-=(div_partitura.getBoundingClientRect().y+div_partitura.getBoundingClientRect().height)-window.innerHeight/2;
	}
	interface_ajustaBarrasGUI();
}

//Ajusta as barras da GUI. É recomendável chamar essa função sempre que a página for movida de alguma forma.
function interface_ajustaBarrasGUI() {
	var offsetBarraRolagemV=document.getElementById("barraFerramentas").getBoundingClientRect().height+20;
	var offsetBarraRolagemH=20;
	var margemBarraRolagemH=document.getElementById('paleta_escManual').getBoundingClientRect().height;
	var tamanhoBarraRolagemV=((window.innerHeight-offsetBarraRolagemV-(60+margemBarraRolagemH))/div_partitura.getBoundingClientRect().height)*(window.innerHeight/2);
	var tamanhoBarraRolagemH=((window.innerWidth-offsetBarraRolagemH-60)/div_partitura.getBoundingClientRect().width)*(window.innerWidth/2);
	var posicaoBarraRolagemV=((-div_partitura.getBoundingClientRect().y+(window.innerHeight/2))/div_partitura.getBoundingClientRect().height)*(window.innerHeight-offsetBarraRolagemV-(40+margemBarraRolagemH)-tamanhoBarraRolagemV);
	var posicaoBarraRolagemH=((-div_partitura.getBoundingClientRect().x+(window.innerWidth/2))/div_partitura.getBoundingClientRect().width)*(window.innerWidth-offsetBarraRolagemH-40-tamanhoBarraRolagemH);
	posicaoBarraRolagemV+=offsetBarraRolagemV;
	posicaoBarraRolagemH+=offsetBarraRolagemH;
	document.getElementById("GUIBotaoRolagemV1").style.top=(offsetBarraRolagemV-20)+"px";
	document.getElementById("GUIBotaoRolagemV2").style.bottom=(margemBarraRolagemH+20)+"px";
	document.getElementById("GUIBotaoRolagemH1").style.bottom=(margemBarraRolagemH)+"px";
	document.getElementById("GUIBotaoRolagemH2").style.bottom=(margemBarraRolagemH)+"px";
	document.getElementById("GUIRolagemH").style.bottom=(margemBarraRolagemH)+"px";
	div_GUIBarraRolagemV.style.top=posicaoBarraRolagemV+"px";
	div_GUIBarraRolagemV.style.height=tamanhoBarraRolagemV+"px";
	div_GUIBarraRolagemH.style.bottom=margemBarraRolagemH+"px";
	div_GUIBarraRolagemH.style.left=posicaoBarraRolagemH+"px";
	div_GUIBarraRolagemH.style.width=tamanhoBarraRolagemH+"px";
}

// === CÓDIGOS DE EXECUÇÃO EM TEMPO REAL === ///////////////////////////////////////////////////////////////////////////
{
//Reajustar a posição da página caso a tela sofra alteração de tamanho
window.onresize=interface_ajustaBarrasGUI;
}
//Comandos de Mouse e Teclado//////////////////////////////////////////////////////////////////////
if (document.addEventListener) {
	document.addEventListener('contextmenu', function(e) {
		origem = e.srcElement || e.target;
		if ((origem.classList.contains("compasso")) || (origem.parentElement.classList.contains("compasso"))) {
			e.preventDefault();
			origem.click();
			interface_showMenuContexto(true,e);
		}
	}, false);
	document.addEventListener('mousedown', function(e) {
		if ((ferramenta=="f_mao")&&(moverPagina==false)) {
			moverPagina=true;
			offsetX=e.clientX;
			offsetY=e.clientY;
			div_partitura.style.cursor="grabbing";
		}
	}, false);
	document.addEventListener('mousemove', function(e) {
		if (moverPagina) {
			paginaOffsetX-=(offsetX-e.clientX)/(zoomPagina/100);
			paginaOffsetY-=(offsetY-e.clientY)/(zoomPagina/100);
			//console.log((offsetX-e.clientX)*(zoomPagina/100));
			interface_ajustaPosicaoPagina();
			offsetX=e.clientX;
			offsetY=e.clientY;
		}
	}, false);
	document.addEventListener('mouseup', function(e) {
		if (moverPagina==true) {
			moverPagina=false;
			if (ferramenta=="f_mao") {
				div_partitura.style.cursor="grab";
			} else {
				div_partitura.style.cursor="unset";
			}
		}
		origem = e.srcElement || e.target;
		if (origem.parentElement!==null) {
			//Clique fora do menu de contexto faz fechá-lo
			if (origem.parentElement.id!=="menuDeContexto") { 
				interface_showMenuContexto(false,e);
				interface_hideSubMenuContexto();
			}
			//Clique em uma figura faz várias ações de acordo com a ferramenta selecionada
			if ((origem.parentElement.classList.contains("compasso")) && (origem.classList.contains("figura"))) { 
				if ((ferramenta=='f_escManual') && (escritaManual=='borracha')) {
					alert("Apagar!");
				}
			}
		}
	}, false);
	document.addEventListener('wheel', function(e) {
		if (e.altKey) {
			paginaOffsetX-=e.deltaY*10;
			paginaOffsetY-=e.deltaX*10;
		} else {
			paginaOffsetX-=e.deltaX*10;
			paginaOffsetY-=e.deltaY*10;
		}
		interface_ajustaPosicaoPagina();
	}, false)
	document.addEventListener('keydown', function(e) {
		e=window.event||e;
		origem = e.keyCode || e.which || e.charCode;
	}, false);
	document.addEventListener('keyup', function(e) {
		e=window.event||e;
		origem = e.keyCode || e.which || e.charCode;
		if (e.altKey) {
			switch (String.fromCharCode(origem)) {
				case "1": interface_selecionarEscritaManual('sfPausa'); break;
				case "2": interface_selecionarEscritaManual('fPausa'); break;
				case "3": interface_selecionarEscritaManual('scPausa'); break;
				case "4": interface_selecionarEscritaManual('cPausa'); break;
				case "5": interface_selecionarEscritaManual('smPausa'); break;
				case "6": interface_selecionarEscritaManual('mPausa'); break;
				case "7": interface_selecionarEscritaManual('sbPausa'); break;
				case "8": interface_selecionarEscritaManual('bPausa'); break;
			}
		} else {
			switch (String.fromCharCode(origem)) {
				case "1": interface_selecionarEscritaManual('sf'); break;
				case "2": interface_selecionarEscritaManual('f'); break;
				case "3": interface_selecionarEscritaManual('sc'); break;
				case "4": interface_selecionarEscritaManual('c'); break;
				case "5": interface_selecionarEscritaManual('sm'); break;
				case "6": interface_selecionarEscritaManual('m'); break;
				case "7": interface_selecionarEscritaManual('sb'); break;
				case "8": interface_selecionarEscritaManual('b'); break;
			}
		}
		if (origem==18) {
			return false;
		}
	}, false);
}